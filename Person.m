//
//  Person.m
//  Task2
//
//  Created by Dmitry Venikov on 22/09/14.
//  Copyright (c) 2014 Dmitry Venikov. All rights reserved.
//

#import "Person.h"

@implementation Person

- (instancetype)initWithFilePath:(NSString *)filePath
{
    self = [super init];
    if (self) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
//  FIXME: it always has object at index 0, you don't have to check it
        if ([paths count]) {
            NSString *path = [[paths objectAtIndex:0]
                                  stringByAppendingPathComponent:filePath];
            NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:path];
//  FIXME: you should have string key for adding fistName and another string key for adding secondName, you don't have to enumerate thought this dictionary, just take value for key
            for (NSString *key in dictionary) {
                self.firstName = key;
                self.lastName = dictionary[key];
                NSLog(@" %@ : %@ ", key, dictionary[key]);
            }
        }
    }
    return self;
}

- (void)saveToFilePath:(NSString *)filePath
{
//  FIXME: you should have string key for adding fistName and another string key for adding secondName
//  FIXME: what if self.firstName == nil and self.lastName == nil?
    NSDictionary *dictionary = @{ self.firstName : self.lastName };
    
    // Save dictionary to application document directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
//  FIXME: it always has object at index 0, you don't have to check it
    if ([paths count]) {
        NSString *path = [[paths objectAtIndex:0]
                    stringByAppendingPathComponent:filePath];
        
        [dictionary writeToFile:path atomically:YES];
    }
}

@end
