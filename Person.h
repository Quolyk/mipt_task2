//
//  Person.h
//  Task2
//
//  Created by Dmitry Venikov on 22/09/14.
//  Copyright (c) 2014 Dmitry Venikov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;

- (void)saveToFilePath:(NSString *)filePath;
- (instancetype)initWithFilePath:(NSString *)filePath;

@end
