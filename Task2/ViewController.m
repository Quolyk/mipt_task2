//
//  ViewController.m
//  Task2
//
//  Created by Dmitry Venikov on 22/09/14.
//  Copyright (c) 2014 Dmitry Venikov. All rights reserved.
//

#import "ViewController.h"
#import "Person.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    Person *firstPerson = [[Person alloc] init];
    
    NSString *filePath = @"file.out";
    firstPerson.firstName = @"Dmitry";
    firstPerson.lastName = @"Venikov";
    [firstPerson saveToFilePath:filePath];
    
    Person *secondPerson = [[Person alloc] initWithFilePath:filePath];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
